package com.codegreenllc;

import java.util.List;

import javax.ejb.Remote;

import com.codegreenllc.entity.CatVB;


@Remote
public interface KenelFacadeBeanRemote {
	public List<Dog> findAllDogs();

	public List<CatVB> findAllCats();

	public CatVB findCatByOid(int oid);

	public CatVB findCatByName(String name);

	public Dog findDogByOid(int id);

}
