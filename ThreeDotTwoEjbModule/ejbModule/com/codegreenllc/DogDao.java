package com.codegreenllc;

import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

@Stateful
public class DogDao {
	@PersistenceContext(unitName = "kenel-unit", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	  public void addDog(Dog dog) throws Exception {
	        entityManager.persist(dog);
	    }

	    public void removeDog(Dog dog) throws Exception {
	        entityManager.remove(dog);
	    }

	    @SuppressWarnings("unchecked")
		public List<Dog> findAllDogs() throws Exception {
	    	Query query = entityManager.createNamedQuery("Dog.findAll");
	        return query.getResultList();
	    }

		public Dog findByOid(int oid) {
			return entityManager.find(Dog.class, oid);
		}
}
