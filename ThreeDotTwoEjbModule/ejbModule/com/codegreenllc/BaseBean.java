package com.codegreenllc;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

/**
 * Session Bean implementation class KenelFacadeBean
 */
@Transactional(value = TxType.REQUIRES_NEW)
public class BaseBean {
}
