package com.codegreenllc.entity;

import javax.ejb.CreateException;
import javax.ejb.EntityBean;
import javax.ejb.EntityContext;

public abstract class CatBean implements EntityBean {

	private static final long serialVersionUID = 1L;

	private EntityContext ctx;

    // container-managed persistent fields accessors
    public abstract Integer getOid();
    public abstract void setOid(Integer oid);
 
    public abstract String getName();
    public abstract void setName(String name);
 
 
//    public void CatBean() {
//        // Empty constructor, don't initialize here but in the create().
//        // passivate() may destroy these attributes in the case of pooling
//    }

    public Integer ejbCreate(Integer oid, String name)
        throws CreateException {
        setOid(oid);
        setName(name);
        return oid;
    }
 
    public Integer ejbCreate(Integer oid)
            throws CreateException {
            setOid(oid);
            return oid;
        }

    public void ejbPostCreate(Integer oid, String name)
        throws CreateException {
        // when just after bean created
    }
    public void ejbPostCreate(Integer oid)
            throws CreateException {
            // when just after bean created
        }
 
    public void ejbStore() {
        // when bean persisted
    }
 
    public void ejbLoad() {
        // when bean loaded
    }
 
    public void ejbRemove() {
        // when bean removed
    }
 
    public void ejbActivate() {
        // when bean activated
    }
 
    public void ejbPassivate() {
        // when bean deactivated
    }
 
    public void setEntityContext(EntityContext ctx) {
        this.ctx = ctx;
    }
 
    public void unsetEntityContext() {
        this.ctx = null;
    }

}