package com.codegreenllc.entity;

import java.io.Serializable;
import java.rmi.RemoteException;

public class CatVB implements Serializable {
	private static final long serialVersionUID = 1L;

	public CatVB(Cat c) {
		try {
			oid = c.getOid();
			name = c.getName();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public CatVB(CatLocal c) {
		oid = c.getOid();
		name = c.getName();
	}

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	Integer oid;
	String name;

	public String toString() {
		return "Cat[oid: " + oid + ", name: " + name + "]";
	}
}
