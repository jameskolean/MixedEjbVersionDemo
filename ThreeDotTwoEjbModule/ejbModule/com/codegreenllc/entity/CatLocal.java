package com.codegreenllc.entity;

import javax.ejb.EJBLocalObject;

public interface CatLocal extends EJBLocalObject {
	public Integer getOid();

	public void setOid(Integer Oid);

	public String getName();

	public void setName(String empName);

}
