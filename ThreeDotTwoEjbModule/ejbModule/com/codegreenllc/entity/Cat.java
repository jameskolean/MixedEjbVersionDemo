package com.codegreenllc.entity;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface Cat extends EJBObject {
	public Integer getOid() throws RemoteException;

	public void setOid(Integer Oid) throws RemoteException;

	public String getName() throws RemoteException;

	public void setName(String empName) throws RemoteException;

}
