package com.codegreenllc.entity;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import javax.ejb.FinderException;

public interface CatHome extends EJBHome {
	public Cat create(Integer Oid, String name) throws CreateException, RemoteException;

	public Cat findByPrimaryKey(Integer oid) throws FinderException, RemoteException;
}
