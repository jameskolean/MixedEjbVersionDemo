package com.codegreenllc.entity;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBLocalHome;
import javax.ejb.FinderException;

public interface CatLocalHome extends EJBLocalHome {
	CatLocal create(Integer oid) throws CreateException;

	public CatLocal findByPrimaryKey(Integer oid) throws FinderException;

	public Collection<?> findByName(String name) throws FinderException;

	public Collection<?> findAll() throws FinderException;

}