package com.codegreenllc;

import javax.ejb.Remote;

@Remote
public interface ThreeDotTwoSessionBeanRemote {
	public String sayHello();

}
