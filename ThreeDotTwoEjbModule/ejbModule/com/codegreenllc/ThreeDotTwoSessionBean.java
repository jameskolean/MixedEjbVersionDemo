package com.codegreenllc;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class ThreeDotTwoSessionBean
 */
@Stateless(mappedName = "ThreeDotTwoSessionBean")
@LocalBean
public class ThreeDotTwoSessionBean implements ThreeDotTwoSessionBeanRemote, ThreeDotTwoSessionBeanLocal {

    /**
     * Default constructor. 
     */
    public ThreeDotTwoSessionBean() {
    }

    @Override
	public String sayHello() {
		return "Hello from 3.2 Session Bean";
	}


}
