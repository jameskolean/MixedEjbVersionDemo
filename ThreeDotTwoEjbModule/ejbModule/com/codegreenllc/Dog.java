package com.codegreenllc;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the DOG database table.
 * 
 */
@Entity
@Table(name="dog")
@NamedQuery(name="Dog.findAll", query="SELECT d FROM Dog d")
public class Dog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer oid;

	private String name;

	public Dog() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

}