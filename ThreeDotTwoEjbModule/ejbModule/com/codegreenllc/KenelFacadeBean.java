package com.codegreenllc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.FinderException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.codegreenllc.entity.CatLocal;
import com.codegreenllc.entity.CatLocalHome;
import com.codegreenllc.entity.CatVB;

/**
 * Session Bean implementation class KenelFacadeBean
 */
@Stateless(mappedName = "KenelFacadeBean")
@LocalBean
public class KenelFacadeBean extends BaseBean implements KenelFacadeBeanRemote, KenelFacadeBeanLocal {

	@EJB
	CatLocalHome catLocalHome;

	@EJB
	DogDao dogDao;

	public List<Dog> findAllDogs() {
		try {
			return dogDao.findAllDogs();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<Dog>();
	}

	@Override
	public Dog findDogByOid(int oid) {
		return dogDao.findByOid(oid);
	}

	@Override
	public List<CatVB> findAllCats() {
		try {
			Collection<?> cats = catLocalHome.findAll();
			List<CatVB> result = cats.stream().map(c -> new CatVB((CatLocal) c)).collect(Collectors.toList());
			return result;
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<CatVB>();
	}

	@Override
	public CatVB findCatByOid(int oid) {
		try {
			return new CatVB((CatLocal) catLocalHome.findByPrimaryKey(oid));
		} catch (FinderException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public CatVB findCatByName(String name) {
		try {
			Collection<?> cats = catLocalHome.findByName(name);
			if (cats.size() > 0) {
				return new CatVB((CatLocal) cats.iterator().next());
			}
		} catch (FinderException e) {
			e.printStackTrace();
		}
		return null;
	}

}
