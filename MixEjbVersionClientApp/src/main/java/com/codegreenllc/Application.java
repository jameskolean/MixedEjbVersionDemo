package com.codegreenllc;

import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import com.codegreenllc.entity.CatVB;

public class Application {

	final static String THREE_TWO_SESSION_REMOTE = "ThreeDotTwoSessionBean#com.codegreenllc.ThreeDotTwoSessionBeanRemote";
	final static String TWO_ONE_SESSION_REMOTE = "java:global.MixedEjbVersionEar.TwoDotOneEjbModule.TwoDotOneSessionBean!com.codegreenllc.TwoDotOneSessionBeanRemoteHome";
	final static String KENEL_REMOTE = "KenelFacadeBean#com.codegreenllc.KenelFacadeBeanRemote";

	private static Properties buildContextProperties() {
		final Properties props = new Properties();
		props.setProperty("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
		props.setProperty("java.naming.provider.url", "t3://localhost:7001");
		props.setProperty("java.naming.security.principal", "weblogic");
		props.setProperty("java.naming.security.credentials", "password1");
		return props;
	}

	private static void callEJB2Session() {
		try {
			final TwoDotOneSessionBeanRemoteHome home = Cast.cast(
					PortableRemoteObject.narrow(lookup(TWO_ONE_SESSION_REMOTE), TwoDotOneSessionBeanRemoteHome.class));
			final TwoDotOneSessionBeanRemoteComponent remote = home.create();
			System.out.printf("callEJB 2.1 Session Bean: %s%n", remote.sayHello());
		} catch (final Exception e) {
			System.out.println(e);
		}
	}

	private static void callKenel() {
		try {
			System.out.println("Cats ***************");
			final KenelFacadeBeanRemote remote = Cast.cast(lookup(KENEL_REMOTE));
			List<CatVB> cats = remote.findAllCats();
			String catNames = cats.stream().map(c -> c.getName()).collect(Collectors.joining(","));
			System.out.printf("callKenel 3.2 Session Bean holds Cats: %s%n", catNames);
			System.out.printf("callKenel 3.2 Session Bean holds Cat with oid 1: %s%n", remote.findCatByOid(1));
			System.out.printf("callKenel 3.2 Session Bean holds Cat with name 'Paws': %s%n",
					remote.findCatByName("Paws"));
			System.out.println("Dogs ***************");
			List<Dog> dogs = remote.findAllDogs();
			String dogNames = dogs.stream().map(d -> d.getName()).collect(Collectors.joining(","));
			System.out.printf("callKenel 3.2 Session Bean holds Dogs: %s%n", dogNames);
		} catch (final Exception e) {
			System.out.println(e);
		}
	}

	private static void callTimingTest() {
		try {
			System.out.println("Starting timing test");
// warm up
			final KenelFacadeBeanRemote remote = Cast.cast(lookup(KENEL_REMOTE));
			IntStream.range(0, 1).forEach(i -> remote.findCatByOid(2));
//			IntStream.range(0, 1).forEach(i -> remote.findDogByOid(2));
			
			System.out.print("calling JPA the first time ... ");
			long startJpa1 = new Date().getTime();
 			remote.findDogByOid(1);
			System.out.println("took "+(new Date().getTime()-startJpa1));

			System.out.print("calling JPA 1000 times ... ");
			long startJpa = new Date().getTime();
 			IntStream.range(0, 1000).forEach(i -> remote.findDogByOid(1));
			System.out.println("took "+(new Date().getTime()-startJpa));
			
			System.out.print("calling Entity the first time ... ");
			long startEnt1 = new Date().getTime();
			remote.findCatByOid(1);
			System.out.println("took "+(new Date().getTime()-startEnt1));

			System.out.print("calling Entity 1000 times ... ");
			long startEnt = new Date().getTime();
			IntStream.range(0, 1000).forEach(i -> remote.findCatByOid(1));
			System.out.println("took "+(new Date().getTime()-startEnt));

} catch (final Exception e) {
			e.printStackTrace();;
		}
	}

	private static void callEJB3Session() {
		try {
			final ThreeDotTwoSessionBeanRemote remote = Cast.cast(lookup(THREE_TWO_SESSION_REMOTE));
			remote.sayHello();
			System.out.printf("callEJB 3.2 Session Bean: %s%n", remote.sayHello());
		} catch (final Exception e) {
			System.out.println(e);
		}
	}

	private static Object lookup(final String jndiName) throws NamingException {
		final InitialContext ctx = new InitialContext(buildContextProperties());
		return ctx.lookup(jndiName);
	}

	public static void main(final String[] args) {
		callTimingTest();
		callEJB2Session();
		callEJB3Session();
		callKenel();
	}

}
