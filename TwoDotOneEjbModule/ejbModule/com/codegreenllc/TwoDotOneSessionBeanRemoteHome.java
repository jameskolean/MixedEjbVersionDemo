package com.codegreenllc;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface TwoDotOneSessionBeanRemoteHome extends EJBHome {

	public TwoDotOneSessionBeanRemoteComponent create() throws CreateException, RemoteException;
	
}
