package com.codegreenllc;

import javax.ejb.EJBLocalObject;

public interface TwoDotOneSessionBeanLocalComponent extends EJBLocalObject {
	public String sayHello();

}
