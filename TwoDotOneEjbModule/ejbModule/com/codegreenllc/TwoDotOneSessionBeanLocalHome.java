package com.codegreenllc;

import javax.ejb.CreateException;
import javax.ejb.EJBLocalHome;

public interface TwoDotOneSessionBeanLocalHome extends EJBLocalHome {

	public TwoDotOneSessionBeanLocalComponent create() throws CreateException;
	
}
