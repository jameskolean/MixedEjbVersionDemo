package com.codegreenllc;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface TwoDotOneSessionBeanRemoteComponent extends EJBObject {
	public String sayHello() throws RemoteException;
}
