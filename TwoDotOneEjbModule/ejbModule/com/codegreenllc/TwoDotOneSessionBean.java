package com.codegreenllc;

import java.rmi.RemoteException;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

public class TwoDotOneSessionBean implements SessionBean {
	private static final long serialVersionUID = -8515963042526627628L;

	@Override
	public void ejbActivate() throws EJBException, RemoteException {
	}

	public void ejbCreate() {
	}

	@Override
	public void ejbPassivate() throws EJBException, RemoteException {
	}

	@Override
	public void ejbRemove() throws EJBException, RemoteException {
	}

	public String sayHello() {
		return "Hello from 2.1 Session Bean";
	}

	@Override
	public void setSessionContext(final SessionContext ctx) throws EJBException, RemoteException {
	}

}
